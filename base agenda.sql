create database agenda_electronica;
use agenda_electronica;

create table fechas_especiales(
id int not null auto_increment primary key,
motivo varchar (50) not null,
fecha date
);

create table personas(
id int not null auto_increment primary key,
nombre varchar (30) not null,
apellido varchar (50) not null,
telefono varchar (9) not null,
dui varchar (10) not null,
direccion varchar (100) not null
);

create table tipo_usuarios(
id int not null auto_increment primary key,
tipo varchar (20) not null
);

create table usuarios(
id int not null auto_increment primary key,
usuario varchar (25)not null,
pass blob,
personas int,
tipo_usuarios int,
foreign key (personas) references personas(id),
foreign key (tipo_usuarios) references tipo_usuarios(id)
);

create table estado(
id int not null auto_increment primary key,
estado varchar (20) not null 
);

create table fases(
id int not null auto_increment primary key,
fase varchar (20) not null,
estado int,
foreign key (estado) references estado(id)
);

create table tarea(
id int not null auto_increment primary key,
nombre varchar (50) not null,
descripcion text,
fecha_in date,
fecha_fin date,
usuarios int,
estados int,
foreign key (usuarios) references usuarios(id),
foreign key (estados) references estado(id)
);

create table detalle_f(
id int not null auto_increment primary key,
fases int,
fecha_cam date not null,
tarea int,
foreign key (tarea) references tarea(id),
foreign key (fases) references fases(id)
);

