/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.agenda.conf;

import com.agenda.impl.DetalleFImpl;
import com.agenda.impl.EstadoImpl;
import com.agenda.impl.FechasEspecialesImpl;
import com.agenda.impl.PersonasImpl;
import com.agenda.impl.TareaImpl;
import com.agenda.impl.TipoUsuarioImpl;
import com.agenda.impl.UsuarioImpl;
import java.util.Properties;
import javax.sql.DataSource;
import org.apache.commons.dbcp2.BasicDataSource;
import org.hibernate.SessionFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBuilder;

@Configuration
@ComponentScan(basePackages = {"com.agenda"})
public class AppContextConf {

    @Bean(name = "dataSource")
    public DataSource getDataSource() {
        BasicDataSource dataSource = new BasicDataSource();
        dataSource.setDriverClassName("com.mysql.jdbc.Driver");
        dataSource.setUrl("jdbc:mysql://localhost:3306/agenda_electronica?useSSL=false");
        dataSource.setUsername("root");
        dataSource.setPassword("root");
        return dataSource;
    }

    private Properties getHibernateProperties() {
        Properties properties = new Properties();
        properties.put("hibernate.dialect", "org.hibernate.dialect.MySQLDialect");
        properties.put("hibernate.format_sql", "hibernate.format_sql");
        return properties;
    }

    @Bean(name = "sessionFactory")
    public SessionFactory getsessionFactory(DataSource dataSource) {
        LocalSessionFactoryBuilder sessionBuilder = new LocalSessionFactoryBuilder(dataSource);
        sessionBuilder.addPackages("com.agenda.model");
        sessionBuilder.addProperties(getHibernateProperties());
        return sessionBuilder.buildSessionFactory();
    }

    @Bean(name = "transactionManager")
    public HibernateTransactionManager getTransactionManager(SessionFactory sessionFactory) {
        HibernateTransactionManager hibernateTransaction = new HibernateTransactionManager(sessionFactory);
        return hibernateTransaction;
    }

    @Bean
    public FechasEspecialesImpl fechasespeciales() {
        return new FechasEspecialesImpl();
    }

    @Bean
    public PersonasImpl personas() {
        return new PersonasImpl();
    }

    @Bean
    public TipoUsuarioImpl tipousuarios() {
        return new TipoUsuarioImpl();
    }

    @Bean
    public UsuarioImpl usuarios() {
        return new UsuarioImpl();
    }

    @Bean
    public EstadoImpl estado() {
        return new EstadoImpl();
    }

    @Bean
    public TareaImpl tarea() {
        return new TareaImpl();
    }

    @Bean
    public DetalleFImpl detallef() {
        return new DetalleFImpl();
    }
}
