package com.agenda.impl;

import com.agenda.model.Tarea;
import com.agenda.util.AbstractFacade;
import com.agenda.util.Dao;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository("tarea")
public class TareaImpl extends AbstractFacade<Tarea> implements Dao<Tarea> {

    @Autowired
    private SessionFactory SF;

    public TareaImpl() {
        super(Tarea.class);
    }

    public TareaImpl(SessionFactory sf, Class<Tarea> entityClass) {
        super(entityClass);
    }

    @Override
    protected SessionFactory sessionFactory() {
        return SF;
    }
}
