/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.agenda.impl;

import com.agenda.model.Personas;
import com.agenda.util.AbstractFacade;
import com.agenda.util.Dao;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository("personas")
public class PersonasImpl extends AbstractFacade<Personas> implements Dao<Personas> {

    @Autowired
    private SessionFactory SF;

    public PersonasImpl() {
        super(Personas.class);
    }

    public PersonasImpl(SessionFactory sf, Class<Personas> entityClass) {
        super(entityClass);
    }

    @Override
    protected SessionFactory sessionFactory() {
        return SF;
    }
}
