
package com.agenda.impl;

import com.agenda.model.TipoUsuarios;
import com.agenda.util.AbstractFacade;
import com.agenda.util.Dao;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository("tipoUsuarios")
public class TipoUsuarioImpl extends AbstractFacade<TipoUsuarios> implements Dao<TipoUsuarios> {

    @Autowired
    private SessionFactory SF;

    public TipoUsuarioImpl() {
        super(TipoUsuarios.class);
    }

    public TipoUsuarioImpl(SessionFactory sf, Class<TipoUsuarios> entityClass) {
        super(entityClass);
    }

    @Override
    protected SessionFactory sessionFactory() {
        return SF;
    }
}
