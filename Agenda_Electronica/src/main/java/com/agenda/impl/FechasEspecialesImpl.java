/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.agenda.impl;

import com.agenda.model.FechasEspeciales;
import com.agenda.util.AbstractFacade;
import com.agenda.util.Dao;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository("fechasEspeciales")
public class FechasEspecialesImpl extends AbstractFacade<FechasEspeciales> implements Dao<FechasEspeciales> {

    @Autowired
    private SessionFactory SF;

    public FechasEspecialesImpl() {
        super(FechasEspeciales.class);
    }

    public FechasEspecialesImpl(SessionFactory sf, Class<FechasEspeciales> entityClass) {
        super(entityClass);
    }

    @Override
    protected SessionFactory sessionFactory() {
        return SF;
    }
}
