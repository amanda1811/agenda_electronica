/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.agenda.impl;

import com.agenda.model.Usuarios;
import com.agenda.util.AbstractFacade;
import com.agenda.util.Dao;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository("usuarios")
public class UsuarioImpl extends AbstractFacade<Usuarios> implements Dao<Usuarios> {

    @Autowired
    private SessionFactory SF;

    public UsuarioImpl() {
        super(Usuarios.class);
    }

    public UsuarioImpl(SessionFactory sf, Class<Usuarios> entityClass) {
        super(entityClass);
    }

    @Override
    protected SessionFactory sessionFactory() {
        return SF;
    }
}
