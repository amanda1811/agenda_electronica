/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.agenda.impl;

import com.agenda.model.Fases;
import com.agenda.util.AbstractFacade;
import com.agenda.util.Dao;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository("fases")
public class FasesImpl extends AbstractFacade<Fases> implements Dao<Fases> {

    @Autowired
    private SessionFactory SF;

    public FasesImpl() {
        super(Fases.class);
    }

    public FasesImpl(SessionFactory sf, Class<Fases> entityClass) {
        super(entityClass);
    }

    @Override
    protected SessionFactory sessionFactory() {
        return SF;
    }
}
