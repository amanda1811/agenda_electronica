/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.agenda.impl;

import com.agenda.model.Estado;
import com.agenda.util.AbstractFacade;
import com.agenda.util.Dao;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository("estado")
public class EstadoImpl extends AbstractFacade<Estado> implements Dao<Estado> {

    @Autowired
    private SessionFactory SF;

    public EstadoImpl() {
        super(Estado.class);
    }

    public EstadoImpl(SessionFactory sf, Class<Estado> entityClass) {
        super(entityClass);
    }

    @Override
    protected SessionFactory sessionFactory() {
        return SF;
    }
}
