package com.agenda.impl;

import com.agenda.model.DetalleF;
import com.agenda.util.AbstractFacade;
import com.agenda.util.Dao;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository("detalleF")
public class DetalleFImpl extends AbstractFacade<DetalleF> implements Dao<DetalleF> {

    @Autowired
    private SessionFactory SF;

    public DetalleFImpl() {
        super(DetalleF.class);
    }

    public DetalleFImpl(SessionFactory sf, Class<DetalleF> entityClass) {
        super(entityClass);

    }

    @Override
    protected SessionFactory sessionFactory() {
        return SF;
    }
}
