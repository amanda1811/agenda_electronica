package com.agenda.model;
// Generated 12-14-2019 08:10:27 AM by Hibernate Tools 4.3.1


import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * TipoUsuarios generated by hbm2java
 */
@Entity
@Table(name="tipo_usuarios"
    ,catalog="agenda_electronica"
)
public class TipoUsuarios  implements java.io.Serializable {


     private int id;
     private String tipo;
     private Set usuarioses = new HashSet(0);

    public TipoUsuarios() {
    }

	
    public TipoUsuarios(int id, String tipo) {
        this.id = id;
        this.tipo = tipo;
    }
    public TipoUsuarios(int id, String tipo, Set usuarioses) {
       this.id = id;
       this.tipo = tipo;
       this.usuarioses = usuarioses;
    }
   
     @Id 

    
    @Column(name="id", unique=true, nullable=false)
    public int getId() {
        return this.id;
    }
    
    public void setId(int id) {
        this.id = id;
    }

    
    @Column(name="tipo", nullable=false, length=20)
    public String getTipo() {
        return this.tipo;
    }
    
    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

@OneToMany(fetch=FetchType.EAGER, mappedBy="tipoUsuarios")
    public Set getUsuarioses() {
        return this.usuarioses;
    }
    
    public void setUsuarioses(Set usuarioses) {
        this.usuarioses = usuarioses;
    }




}


