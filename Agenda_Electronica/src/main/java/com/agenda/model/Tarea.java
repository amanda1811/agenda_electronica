package com.agenda.model;
// Generated 12-14-2019 08:10:27 AM by Hibernate Tools 4.3.1


import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Tarea generated by hbm2java
 */
@Entity
@Table(name="tarea"
    ,catalog="agenda_electronica"
)
public class Tarea  implements java.io.Serializable {


     private int id;
     private Estado estado;
     private Usuarios usuarios;
     private String nombre;
     private String descripcion;
     private Date fechaIn;
     private Date fechaFin;
     private Set detalleFs = new HashSet(0);

    public Tarea() {
    }

	
    public Tarea(int id, String nombre) {
        this.id = id;
        this.nombre = nombre;
    }
    public Tarea(int id, Estado estado, Usuarios usuarios, String nombre, String descripcion, Date fechaIn, Date fechaFin, Set detalleFs) {
       this.id = id;
       this.estado = estado;
       this.usuarios = usuarios;
       this.nombre = nombre;
       this.descripcion = descripcion;
       this.fechaIn = fechaIn;
       this.fechaFin = fechaFin;
       this.detalleFs = detalleFs;
    }
   
     @Id 

    
    @Column(name="id", unique=true, nullable=false)
    public int getId() {
        return this.id;
    }
    
    public void setId(int id) {
        this.id = id;
    }

@ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="estados")
    public Estado getEstado() {
        return this.estado;
    }
    
    public void setEstado(Estado estado) {
        this.estado = estado;
    }

@ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="usuarios")
    public Usuarios getUsuarios() {
        return this.usuarios;
    }
    
    public void setUsuarios(Usuarios usuarios) {
        this.usuarios = usuarios;
    }

    
    @Column(name="nombre", nullable=false, length=50)
    public String getNombre() {
        return this.nombre;
    }
    
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    
    @Column(name="descripcion", length=65535)
    public String getDescripcion() {
        return this.descripcion;
    }
    
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Temporal(TemporalType.DATE)
    @Column(name="fecha_in", length=10)
    public Date getFechaIn() {
        return this.fechaIn;
    }
    
    public void setFechaIn(Date fechaIn) {
        this.fechaIn = fechaIn;
    }

    @Temporal(TemporalType.DATE)
    @Column(name="fecha_fin", length=10)
    public Date getFechaFin() {
        return this.fechaFin;
    }
    
    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }

@OneToMany(fetch=FetchType.EAGER, mappedBy="tarea")
    public Set getDetalleFs() {
        return this.detalleFs;
    }
    
    public void setDetalleFs(Set detalleFs) {
        this.detalleFs = detalleFs;
    }




}


