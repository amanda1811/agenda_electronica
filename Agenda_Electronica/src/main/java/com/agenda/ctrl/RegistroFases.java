/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.agenda.ctrl;

import com.agenda.impl.EstadoImpl;
import com.agenda.impl.FasesImpl;
import com.agenda.model.Estado;
import com.agenda.model.Fases;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@ManagedBean
@SessionScoped
@Component
public class RegistroFases implements Serializable {

    @Autowired
    private FasesImpl fasesDao;

    @Autowired
    private EstadoImpl estadoDao;

    private Fases fases;
    private Estado estado;

    private List<Fases> listaFases;
    private List<Estado> listaEstado;

    String mensaje = "";

    public Fases getFases() {
        return fases;
    }

    public void setFases(Fases fases) {
        this.fases = fases;
    }

    public Estado getEstado() {
        return estado;
    }

    public void setEstado(Estado estado) {
        this.estado = estado;
    }

    public List<Fases> getListaFases() {
        this.listaFases = fasesDao.findAll();
        return listaFases;
    }

    public void setListaFases(List<Fases> listaFases) {
        this.listaFases = listaFases;
    }

    public List<Estado> getListaEstado() {
        this.listaEstado = estadoDao.findAll();
        return listaEstado;
    }

    public void setListaEstado(List<Estado> listaEstado) {
        this.listaEstado = listaEstado;
    }

    @PostConstruct
    public void init() {
        estado = new Estado();
        fases = new Fases();
    }

    public void crearEstado() {
        try {
            estadoDao.create(estado);
            mensaje = "Estado Creado";
        } catch (Exception e) {
            mensaje = "Error:" + e.getMessage();
            e.printStackTrace();
        }

        FacesMessage msj = new FacesMessage(this.mensaje);
        FacesContext.getCurrentInstance().addMessage(null, msj);
    }

    public void editarEstado() {
        try {
            estadoDao.edit(estado);
            mensaje = "Estado Editado";
        } catch (Exception e) {
            mensaje = "Error:" + e.getMessage();
            e.printStackTrace();
        }

        FacesMessage msj = new FacesMessage(this.mensaje);
        FacesContext.getCurrentInstance().addMessage(null, msj);
    }

    public void eliminarEstado(Estado es) {
        try {
            estadoDao.remove(es);
            mensaje = "Estado Eliminado";
        } catch (Exception e) {
            mensaje = "Error:" + e.getMessage();
            e.printStackTrace();
        }

        FacesMessage msj = new FacesMessage(this.mensaje);
        FacesContext.getCurrentInstance().addMessage(null, msj);
    }

    public void cargarEstado(Estado es) {
        estado = es;

    }

    public void crearFases() {
        try {
            fases.setEstado(estado);
            fasesDao.create(fases);
            mensaje = "Fase Creada";
        } catch (Exception e) {
            mensaje = "Error:" + e.getMessage();
            e.printStackTrace();
        }
        
        FacesMessage msj = new FacesMessage(this.mensaje);
        FacesContext.getCurrentInstance().addMessage(null, msj);
    }

    public void editarFases() {
        try {
            fases.setEstado(estado);
            fasesDao.edit(fases);
            mensaje = "Fase Editada";
        } catch (Exception e) {
            mensaje = "Error:" + e.getMessage();
            e.printStackTrace();
        }
        FacesMessage msj = new FacesMessage(this.mensaje);
        FacesContext.getCurrentInstance().addMessage(null, msj);
    }

    public void eliminarFases(Fases fa) {
        try {
            fasesDao.remove(fa);
            mensaje = "Fase Eliminada";
        } catch (Exception e) {
            mensaje = "Error:" + e.getMessage();
            e.printStackTrace();
        }
        
        FacesMessage msj = new FacesMessage(this.mensaje);
        FacesContext.getCurrentInstance().addMessage(null, msj);
    }

    public void cargarFase(Fases fa) {
        estado.setId(fa.getId());
        fases = fa;
    }
}
