/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.agenda.ctrl;

import com.agenda.impl.EstadoImpl;
import com.agenda.impl.TareaImpl;
import com.agenda.impl.UsuarioImpl;
import com.agenda.model.Estado;
import com.agenda.model.Tarea;
import com.agenda.model.Usuarios;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@ManagedBean
@SessionScoped
public class RegistroTareas implements Serializable {

    @Autowired
    private UsuarioImpl usuariosDao;

    @Autowired
    private EstadoImpl estadoDao;

    private TareaImpl tareaDao;

    private Usuarios usuarios;
    private Estado estado;
    private Tarea tarea;

    private List<Tarea> listaTarea;

    String mensaje = "";

    public Usuarios getUsuarios() {
        return usuarios;
    }

    public void setUsuarios(Usuarios usuarios) {
        this.usuarios = usuarios;
    }

    public Estado getEstado() {
        return estado;
    }

    public void setEstado(Estado estado) {
        this.estado = estado;
    }

    public Tarea getTarea() {
        return tarea;
    }

    public void setTarea(Tarea tarea) {
        this.tarea = tarea;
    }

    public List<Tarea> getListaTarea() {
        this.listaTarea = tareaDao.findAll();
        return listaTarea;
    }

    public void setListaTarea(List<Tarea> listaTarea) {
        this.listaTarea = listaTarea;
    }

    @PostConstruct
    public void init() {
        tarea = new Tarea();
        estado = new Estado();
        usuarios = new Usuarios();
    }

    public void crearTarea(){
        try {
            tarea.setUsuarios(usuarios);
            tarea.setEstado(estado);
            tareaDao.create(tarea);
            mensaje="Tarea Creada";
        } catch (Exception e) {
            mensaje="Error:"+e.getMessage();
            e.printStackTrace();
        }
        
        FacesMessage msj = new FacesMessage(this.mensaje);
        FacesContext.getCurrentInstance().addMessage(null, msj);
    }
    public void editarTarea(){
        try {
            tarea.setUsuarios(usuarios);
            tarea.setEstado(estado);
            tareaDao.edit(tarea);
            mensaje="Tarea Editada";
        } catch (Exception e) {
            mensaje="Error:"+e.getMessage();
            e.printStackTrace();
        }
        
        FacesMessage msj = new FacesMessage(this.mensaje);
        FacesContext.getCurrentInstance().addMessage(null, msj);
    }
    
    
    public void eliminarTarea(Tarea ta){
        try {
            tareaDao.remove(ta);
            mensaje="Tarea Eliminada";
        } catch (Exception e) {
            mensaje="Error:"+e.getMessage();
            e.printStackTrace();
        }
        
        FacesMessage msj = new FacesMessage(this.mensaje);
        FacesContext.getCurrentInstance().addMessage(null, msj);
    }
}
