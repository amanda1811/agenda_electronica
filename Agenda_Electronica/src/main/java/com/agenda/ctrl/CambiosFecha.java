/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.agenda.ctrl;

import com.agenda.impl.DetalleFImpl;
import com.agenda.impl.FasesImpl;
import com.agenda.impl.TareaImpl;
import com.agenda.model.DetalleF;
import com.agenda.model.Fases;
import com.agenda.model.Tarea;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@ManagedBean
@SessionScoped
public class CambiosFecha implements Serializable {

    @Autowired
    private TareaImpl tareaDao;

    @Autowired
    private FasesImpl fasesDao;

    @Autowired
    private DetalleFImpl detalleDao;

    private Tarea tarea;
    private Fases fases;
    private DetalleF detalleFecha;

    private List<DetalleF> listaDetalle;
    private List<Fases> listaFases;
    private List<Tarea> listaTarea;
    String mensaje = "";

    public Tarea getTarea() {
        return tarea;
    }

    public void setTarea(Tarea tarea) {
        this.tarea = tarea;
    }

    public Fases getFases() {
        return fases;
    }

    public void setFases(Fases fases) {
        this.fases = fases;
    }

    public DetalleF getDetalleFecha() {
        return detalleFecha;
    }

    public void setDetalleFecha(DetalleF detalleFecha) {
        this.detalleFecha = detalleFecha;
    }

    public List<DetalleF> getListaDetalle() {
        this.listaDetalle = detalleDao.findAll();
        return listaDetalle;
    }

    public void setListaDetalle(List<DetalleF> listaDetalle) {
        this.listaDetalle = listaDetalle;
    }

    public List<Fases> getListaFases() {
        this.listaFases= fasesDao.findAll();
        return listaFases;
    }

    public void setListaFases(List<Fases> listaFases) {
        this.listaFases = listaFases;
    }

    public List<Tarea> getListaTarea() {
        this.listaTarea= tareaDao.findAll();
        return listaTarea;
    }

    public void setListaTarea(List<Tarea> listaTarea) {
        this.listaTarea = listaTarea;
    }

    
    
    @PostConstruct
    public void init() {
        tarea = new Tarea();
        fases = new Fases();
        detalleFecha = new DetalleF();
    }

    public void crearDetalleFecha() {
        try {
            detalleFecha.setFases(fases);
            detalleFecha.setTarea(tarea);
            detalleDao.create(detalleFecha);
            mensaje = "Detalle Creado";
        } catch (Exception e) {
            mensaje = "Error:" + e.getMessage();
            e.printStackTrace();
        }
        FacesMessage msj = new FacesMessage(this.mensaje);
        FacesContext.getCurrentInstance().addMessage(null, msj);
    }

    public void editarDetalleFecha() {
        try {
            detalleFecha.setFases(fases);
            detalleFecha.setTarea(tarea);
            detalleDao.edit(detalleFecha);
            mensaje = "Detalle Editado";
        } catch (Exception e) {
            mensaje = "Error:" + e.getMessage();
            e.printStackTrace();
        }
        FacesMessage msj = new FacesMessage(this.mensaje);
        FacesContext.getCurrentInstance().addMessage(null, msj);
    }

    public void eliminarDetalleFecha(DetalleF df) {
        try {
            detalleDao.remove(df);
            mensaje = "Detalle Eliminado";
        } catch (Exception e) {
            mensaje = "Error:" + e.getMessage();
            e.printStackTrace();
        }
        FacesMessage msj = new FacesMessage(this.mensaje);
        FacesContext.getCurrentInstance().addMessage(null, msj);
    }

    public void cargarDetalle(DetalleF df) {
        fases.setId(df.getId());
        tarea.setId(df.getId());
        detalleFecha = df;
    }
    
}
