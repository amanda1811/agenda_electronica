/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.agenda.ctrl;

import com.agenda.impl.PersonasImpl;
import com.agenda.impl.TipoUsuarioImpl;
import com.agenda.impl.UsuarioImpl;
import com.agenda.model.Personas;
import com.agenda.model.TipoUsuarios;
import com.agenda.model.Usuarios;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import org.springframework.beans.factory.annotation.Autowired;

@ManagedBean
@SessionScoped
public class RegistroPersonas {

    @Autowired
    private TipoUsuarioImpl tipoUsuarioDao;

    @Autowired
    private PersonasImpl personaDao;

    @Autowired
    private UsuarioImpl usuarioDao;

    private TipoUsuarios tipoUsuarios;
    private Personas personas;
    private Usuarios usuarios;

    private List<TipoUsuarios> listaTS;
    private List<Personas> listaPersonas;
    private List<Usuarios> listaUser;

    String mensaje = "";

    public TipoUsuarios getTipoUsuarios() {
        return tipoUsuarios;
    }

    public void setTipoUsuarios(TipoUsuarios tipoUsuarios) {
        this.tipoUsuarios = tipoUsuarios;
    }

    public Personas getPersonas() {
        return personas;
    }

    public void setPersonas(Personas personas) {
        this.personas = personas;
    }

    public Usuarios getUsuarios() {
        return usuarios;
    }

    public void setUsuarios(Usuarios usuarios) {
        this.usuarios = usuarios;
    }

    public List<TipoUsuarios> getListaTS() {
        this.listaTS = tipoUsuarioDao.findAll();
        return listaTS;
    }

    public void setListaTS(List<TipoUsuarios> listaTS) {
        this.listaTS = listaTS;
    }

    public List<Personas> getListaPersonas() {
        this.listaPersonas = personaDao.findAll();
        return listaPersonas;
    }

    public void setListaPersonas(List<Personas> listaPersonas) {
        this.listaPersonas = listaPersonas;
    }

    public List<Usuarios> getListaUser() {
        this.listaUser = usuarioDao.findAll();
        return listaUser;
    }

    public void setListaUser(List<Usuarios> listaUser) {
        this.listaUser = listaUser;
    }

    @PostConstruct
    public void init() {
        tipoUsuarios = new TipoUsuarios();
        usuarios = new Usuarios();
        personas = new Personas();
    }

    //CRUD TIPO USUARIOS
    public void crearTipoUsuario() {
        try {
            tipoUsuarioDao.create(tipoUsuarios);
            mensaje = "Tipo de Usuario Creado";
        } catch (Exception e) {
            mensaje = "Error:" + e.getMessage();
            e.printStackTrace();
        }
        FacesMessage msj = new FacesMessage(this.mensaje);
        FacesContext.getCurrentInstance().addMessage(null, msj);
    }

    public void editarTipoUsuario() {
        try {
            tipoUsuarioDao.edit(tipoUsuarios);
            mensaje = "Tipo de Usuario Editado" + tipoUsuarios.getTipo();
        } catch (Exception e) {
            mensaje = "Error:" + e.getMessage();
            e.printStackTrace();
        }

        FacesMessage msj = new FacesMessage(this.mensaje);
        FacesContext.getCurrentInstance().addMessage(null, msj);
    }

    public void eliminarTipoUsuario(TipoUsuarios ts) {
        try {
            tipoUsuarioDao.remove(ts);
            mensaje = "Eliminado";
        } catch (Exception e) {
            mensaje = "Error:" + e.getMessage();
            e.printStackTrace();
        }
        FacesMessage msj = new FacesMessage(this.mensaje);
        FacesContext.getCurrentInstance().addMessage(null, msj);
    }

    public void cargarTS(TipoUsuarios ts) {
        tipoUsuarios = ts;
    }

    //CRUD PERSONAS
    public void crearPersona() {
        try {
            personaDao.create(personas);
            mensaje = "Persona Creada";
        } catch (Exception e) {
            mensaje = "Error:" + e.getMessage();
            e.printStackTrace();
        }
        FacesMessage msj = new FacesMessage(this.mensaje);
        FacesContext.getCurrentInstance().addMessage(null, msj);
    }

    public void editarPersona() {
        try {
            personaDao.edit(personas);
            mensaje = "Persona Editada";
        } catch (Exception e) {
            mensaje = "Error:" + e.getMessage();
            e.printStackTrace();
        }
        FacesMessage msj = new FacesMessage(this.mensaje);
        FacesContext.getCurrentInstance().addMessage(null, msj);
    }

    public void eliminarPersona(Personas pe) {
        try {
            personaDao.remove(pe);
            mensaje = "Persona Eliminada" + personas.getNombre();
        } catch (Exception e) {
            mensaje = "Error:" + e.getMessage();
            e.printStackTrace();
        }
        FacesMessage msj = new FacesMessage(this.mensaje);
        FacesContext.getCurrentInstance().addMessage(null, msj);
    }

    public void cargarPersona(Personas pe) {
        personas = pe;
    }

    //CRUD USUARIOS
    public void crearUsuario() {
        try {
            usuarios.setPersonas(personas);
            usuarios.setTipoUsuarios(tipoUsuarios);
            usuarioDao.create(usuarios);
            mensaje = "Usuario Registrado";
        } catch (Exception e) {
            mensaje = "Error:" + e.getMessage();
            e.printStackTrace();
        }
        FacesMessage msj = new FacesMessage(this.mensaje);
        FacesContext.getCurrentInstance().addMessage(null, msj);
    }

    public void editarUsuario() {
        try {
            usuarios.setTipoUsuarios(tipoUsuarios);
            usuarios.setPersonas(personas);
            usuarioDao.edit(usuarios);
            mensaje = "Usuario Editado";
        } catch (Exception e) {
            mensaje = "Error:" + e.getMessage();
            e.printStackTrace();
        }
        FacesMessage msj = new FacesMessage(this.mensaje);
        FacesContext.getCurrentInstance().addMessage(null, msj);
    }

    public void eliminarUser(Usuarios us) {
        try {
            usuarioDao.remove(us);
            mensaje = "Usuario Eliminado" + us.getUsuario();
        } catch (Exception e) {
            mensaje = "Error:" + e.getMessage();
            e.printStackTrace();
        }

        FacesMessage msj = new FacesMessage(this.mensaje);
        FacesContext.getCurrentInstance().addMessage(null, msj);

    }

    public void cargarUsuer(Usuarios us) {
        tipoUsuarios.setId(us.getId());
        personas.setId(us.getId());
        usuarios = us;
    }
}
