/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.agenda.ctrl;

import com.agenda.impl.FechasEspecialesImpl;
import com.agenda.model.FechasEspeciales;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@ManagedBean
@SessionScoped
public class Fechas implements Serializable {

    @Autowired
    private FechasEspecialesImpl fechasDao;
    private FechasEspeciales fechasEspeciales;
    private List<FechasEspeciales> listaFechas;

    String mensaje;

    public FechasEspeciales getFechasEspeciales() {
        return fechasEspeciales;
    }

    public void setFechasEspeciales(FechasEspeciales fechasEspeciales) {
        this.fechasEspeciales = fechasEspeciales;
    }

    public List<FechasEspeciales> getListaFechas() {
        this.listaFechas = fechasDao.findAll();
        return listaFechas;
    }

    public void setListaFechas(List<FechasEspeciales> listaFechas) {
        this.listaFechas = listaFechas;
    }

    @PostConstruct
    public void init() {
        fechasEspeciales = new FechasEspeciales();
    }

    public void crearFecha() {
        try {
            fechasDao.create(fechasEspeciales);
            mensaje = "Fecha Creada";
        } catch (Exception e) {
            e.printStackTrace();
        }
        FacesMessage msj = new FacesMessage(this.mensaje);
        FacesContext.getCurrentInstance().addMessage(null, msj);
    }

    public void editarFecha() {
        try {
            fechasDao.edit(fechasEspeciales);
            mensaje = "Fecha Editada";
        } catch (Exception e) {
            e.printStackTrace();
        }
        FacesMessage msj = new FacesMessage(this.mensaje);
        FacesContext.getCurrentInstance().addMessage(null, msj);
    }

    public void eliminarFecha(FechasEspeciales fe) {
        try {
            fechasDao.remove(fe);
            mensaje = "Fecha Eliminada";
        } catch (Exception e) {
            e.printStackTrace();
        }
        FacesMessage msj = new FacesMessage(this.mensaje);
        FacesContext.getCurrentInstance().addMessage(null, msj);
    }

    public void cargarFecha(FechasEspeciales fe) {
        fechasEspeciales = fe;
    }
}
